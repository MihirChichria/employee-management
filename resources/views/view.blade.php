@extends('layouts.app')
{{--{{dd($employees)}}--}}
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <p>Employees</p>
                    <a href="{{route('employee.create')}}" class="btn btn-primary">Create New Employee</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive table-striped">
                    <table class="table table-hover table-bordered" id="employees-datatable" width="100%" cellspacing="0">

                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Primary Mobile Number</th>
                            <th>Primary Whatsapp Number</th>
                            <th>Primary Email Number</th>
                            <th width="30%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{$employee->name}}</td>
                                <td>{{$employee->address->address_1}}</td>
                                <td>{{(($employee->primaryMobileNumber() != [])) ? $employee->primaryMobileNumber()[0]->number : 'No Mobile Number'}}</td>
                                <td>{{(($employee->primaryWhatsappNumber() != [])) ? $employee->primaryWhatsappNumber()[0]->number : 'No Whatsapp Number'}}</td>
                                <td>{{(($employee->primaryEmailAddress() != [])) ? $employee->primaryEmailAddress()[0]->email : 'No Primary Email'}}</td>
                                <td>
                                    <a href="{{route('employee.edit', $employee)}}" class="btn btn-primary btn-sm">Edit</a>
                                    <a href="" class="btn btn-danger btn-sm"
                                           data-toggle="modal"
                                           onclick="displayModalForm({{$employee}})" data-target="#deleteModal">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" METHOD="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Are you sure you want to delete Employee Details?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>

    <script>
        function displayModalForm($employee) {
            var url = '/delete/' + $employee.id;
            $("#deleteForm").attr('action', url);
        }
        window.onload = function(e) {
            $('#employees-datatable').dataTable({
                "order": [
                    [1, "ASC"]
                ],
                "columnDefs": [{
                    'orderable': false,
                    'targets': [-1]
                }]
            });
        }
    </script>
@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
@endsection

