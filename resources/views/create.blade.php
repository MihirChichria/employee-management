@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Create Employee</div>

            <form id="employee_create_form" action="{{route('employee.store')}}" method="POST">
                @csrf
                <div class="form-group m-3">
                    <label for="">Employee Name</label>
                    <input id="name" type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Employee Name">
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group m-3">
                    <label class="text-primary" for="">Address Details</label>
                    <div class="row my-2">
                        <div class="col-md-4">
                            <label for="">Address 1*</label>
                            <input id="address_1" type="text" name="address_1" value="{{ old('address_1') }}" class="form-control" placeholder="Address 1">
                            @error('address_1')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">Address 2</label>
                            <input id="address_2" type="text" name="address_2" value="{{ old('address_2') }}" class="form-control" placeholder="Address 2">
                            @error('address_2')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">Location</label>
                            <input id="location" type="text" name="location" value="{{ old('location') }}" class="form-control" placeholder="Location">
                            @error('location')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row my-2">
                        <div class="col-md-4">
                            <label for="">Zip/Postal Code</label>
                            <input id="postal_code" type="number" name="postal_code" value="{{ old('postal_code') }}" class="form-control" placeholder="000000">
                            @error('postal_code')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">Postal Area</label>
                            <input id="postal_area" type="text" name="postal_area" value="{{ old('postal_area') }}" class="form-control" placeholder="Postal Area">
                            @error('postal_area')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">Taluka</label>
                            <input id="taluka" type="text" name="taluka" value="{{ old('taluka') }}" class="form-control" placeholder="Taluka">
                            @error('taluka')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row my-2">
                        <div class="col-md-4">
                            <label for="">Suburb</label>
                            <input id="suburb" type="text" name="suburb" value="{{ old('suburb') }}" class="form-control" placeholder="Suburb">
                            @error('suburb')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">East/West</label>
                            <input id="direction" type="text" name="direction" value="{{ old('direction') }}" class="form-control" placeholder="Direction">
                            @error('direction')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">City*</label>
                            <input id="city" type="text" name="city" value="{{ old('city') }}" class="form-control" placeholder="City">
                            @error('city')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row my-2">
                        <div class="col-md-4">
                            <label for="">District</label>
                            <input id="district" type="text" name="district" value="{{ old('district') }}" class="form-control" placeholder="District">
                            @error('district')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">State</label>
                            <input id="state" type="text" name="state" value="{{ old('state') }}" class="form-control" placeholder="State">
                            @error('state')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="">Country</label>
                            <input id="country" type="text" name="country" value="{{ old('country') }}" class="form-control" placeholder="Country">
                            @error('country')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group m-3">
                    <label class="text-primary" for="">Contact Details</label>
                    <div class="row my-2">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="">Mobile Number</label>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" onclick="add(this)" id="add_mobile_number" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                                </div>

                            </div>
                            <hr>
                            <div class="mobile_number_input_container">
                                <div id="mobile_1" class="mt-3">
                                    <button type="button" onclick="deleteElement(1, this)" class="delete_mobile btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    <div class="float-right">
                                        <input type="radio" name="primary_mobile" id="mobile_radio_1" value="1">
                                        <label for="mobile_radio_1">Primary</label>
                                    </div>
                                    <input class="form-control" type="number" name="mobile_number[]" id="mobile_1" placeholder="0000000000">
                                </div>
                            </div>
                            @error('mobile_number')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="">Whatsapp Number</label>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" onclick="add(this)" id="add_whatsapp_number" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                                </div>

                            </div>
                            <hr>
                            <div class="whatsapp_number_input_container mt-3">
                                <div id="whatsapp_1" class="mt-3">
                                    <button type="button" onclick="deleteElement(1, this)" class="delete_whatsapp btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    <div class="float-right">
                                        <input type="radio" name="primary_whatsapp" id="whatsapp_radio_1" value="1">
                                        <label for="whatsapp_radio_1">Primary</label>
                                    </div>
                                    <input class="form-control" type="number" name="whatsapp_number[]" id="whatsapp_1" placeholder="0000000000">
                                </div>
                            </div>
                            @error('whatsapp_number')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="">Email Address*</label>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" onclick="add(this)" id="add_email_address" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <hr>
                            <div class="email_address_input_container mt-3">
                                <div id="email_1" class="mt-3">
                                    <button type="button" onclick="deleteElement(1, this)" class="delete_email btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    <div class="float-right">
                                        <input type="radio" name="primary_email" id="email_radio_1" value="1">
                                        <label for="email_radio_1">Primary</label>
                                    </div>
                                    <input class="form-control" type="text" name="email[]" id="email_1" placeholder="xyz@abc.com">
                                </div>
                            </div>
                            @error('email')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-5">
                            <button class="btn btn-primary btn-block" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script>
        var email_count=1, mobile_count=1, whatsapp_count=1;
        function add(button) {
            if(button.id == "add_email_address"){
                email_count++;
                $('.email_address_input_container').append(`
                    <div id="email_${email_count}" class="mt-3">
                        <button type="button" onclick="deleteElement(${email_count}, this)" class="delete_email btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                        <div class="float-right">
                            <input type="radio" name="primary_email" id="email_radio_${email_count}" value="${email_count}">
                            <label for="email_radio_${email_count}">Primary</label>
                        </div>
                        <input class="form-control" type="text" name="email[]" id="email_${email_count}" placeholder="xyz@abc.com">
                    </div>`);
            }
            else if(button.id == "add_whatsapp_number"){
                whatsapp_count++;

                $('.whatsapp_number_input_container').append(`
                    <div id="whatsapp_${whatsapp_count}" class="mt-3">
                    <button type="button" onclick="deleteElement(${whatsapp_count}, this)" class="delete_whatsapp btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                    <div class="float-right">
                    <input type="radio" name="primary_whatsapp" id="whatsapp_radio_${whatsapp_count}" value="${whatsapp_count}">
                    <label for="whatsapp_radio_${whatsapp_count}">Primary</label>
                    </div>
                    <input class="form-control" type="number" name="whatsapp_number[]" id="whatsapp_${whatsapp_count}" placeholder="0000000000">
                    </div>`);
            }
            else {
                  //add_mobile_number
                mobile_count++;
                $('.mobile_number_input_container').append(`
                    <div id="mobile_${mobile_count}" class="mt-3">
                        <button type="button" onclick="deleteElement(${mobile_count}, this)" class="delete_mobile btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                        <div class="float-right">
                          <input type="radio" name="primary_mobile" id="mobile_radio_${mobile_count}" value="${mobile_count}">
                          <label for="mobile_radio_${mobile_count}">Primary</label>
                        </div>
                        <input class="form-control" type="number" name="mobile_number[]" id="mobile_${mobile_count}" placeholder="0000000000">
                    </div>`);
            }
        }

        // function delete(id, element) {
        function deleteElement(id, element) {
            if(element.classList.contains('delete_mobile')){
                var element_id = $(element).parent()[0].id.split('_')[1];

                $("#mobile_"+element_id).remove();
            }
            else if(element.classList.contains('delete_email')){
                var element_id = $(element).parent()[0].id.split('_')[1];
                console.log(element_id)
                $("#email_"+element_id).remove();
            } else{
                var element_id = $(element).parent()[0].id.split('_')[1];
                $("#whatsapp_"+element_id).remove();
            }
        }

        window.onload = function(e){
            $('#employee_create_form').submit(function (event) {
                var radioButton = $("input[name='primary_mobile']:checked")[0];
                radioButton.value = $($("input[name='primary_mobile']:checked")[0]).parent().next().val();
                radioButton = $("input[name='primary_whatsapp']:checked")[0];
                radioButton.value = $($("input[name='primary_whatsapp']:checked")[0]).parent().next().val();
                radioButton = $("input[name='primary_email']:checked")[0];
                radioButton.value = $($("input[name='primary_email']:checked")[0]).parent().next().val();
            });
        }

    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
