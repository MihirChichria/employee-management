<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    //
    protected $fillable = ['name'];

    public function contacts(){
        return $this->hasMany(Contact::class);
    }

    public function address(){
        return $this->hasOne(Address::class);
    }

    public function whatsappNumbers(){
        return DB::select('SELECT contact_whatsapp_number.number, contact_whatsapp_number.isPrimary FROM contact_whatsapp_number, employees, contacts WHERE contact_whatsapp_number.id = contacts.contact_id AND contacts.contact_type = 2 AND contacts.employee_id = employees.id AND employees.id = '. $this->id);
    }

    public function mobileNumbers(){
        return DB::select('SELECT contact_mobile_number.number, contact_mobile_number.isPrimary FROM contact_mobile_number, employees, contacts WHERE contact_mobile_number.id = contacts.contact_id AND contacts.contact_type = 1 AND contacts.employee_id = employees.id AND employees.id = '. $this->id);
    }

    public function emailAddresses(){
        return DB::select('SELECT email_address.email, email_address.isPrimary FROM email_address, employees, contacts WHERE email_address.id = contacts.contact_id AND contacts.contact_type = 3 AND contacts.employee_id = employees.id AND employees.id = '. $this->id);
    }

    public function primaryEmailAddress(){
        return DB::select('SELECT email_address.email, email_address.isPrimary FROM email_address, employees, contacts WHERE email_address.id = contacts.contact_id AND email_address.isPrimary = 1 AND contacts.contact_type = 3 AND contacts.employee_id = employees.id AND employees.id = '. $this->id);
    }

    public function primaryWhatsappNumber(){
        return DB::select('SELECT contact_whatsapp_number.number, contact_whatsapp_number.isPrimary FROM contact_whatsapp_number, employees, contacts WHERE contact_whatsapp_number.id = contacts.contact_id AND contact_whatsapp_number.isPrimary = 1 AND contacts.contact_type = 2 AND contacts.employee_id = employees.id AND employees.id = '. $this->id);
    }

    public function primaryMobileNumber(){
        return DB::select('SELECT contact_mobile_number.number, contact_mobile_number.isPrimary FROM contact_mobile_number, employees, contacts WHERE contact_mobile_number.id = contacts.contact_id AND contact_mobile_number.isPrimary = 1 AND contacts.contact_type = 1 AND contacts.employee_id = employees.id AND employees.id = '. $this->id);
    }
}
