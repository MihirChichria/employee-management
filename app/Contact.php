<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $fillable = ['employee_id', 'contact_type', 'contact_id'];

    public function employee(){
        return $this->belongsTo(Employee::class);
    }
}
