<?php

namespace App\Http\Controllers;

use App\Address;
use App\Employee;
use App\Contact;
use App\Http\Requests\CreateEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function edit(Employee $employee) {
        $address = $employee->address;
        $whatsappNumbers = $employee->whatsappNumbers();
        $mobileNumbers = $employee->mobileNumbers();
        $emailAddresses = $employee->emailAddresses();
        return view('edit', compact([
            'employee',
            'address',
            'whatsappNumbers',
            'mobileNumbers',
            'emailAddresses'
        ]));
    }
    public function update(UpdateEmployeeRequest $request, Employee $employee){
//        dd($request);
        $employee->update(["name"=>$request->name]);
        $employee->address()->update([
            "address_1"=>$request->address_1,
            "address_2"=>$request->address_2,
            "location"=>$request->location,
            "postal_code"=>$request->postal_code,
            "postal_area"=>$request->postal_area,
            "taluka"=>$request->taluka,
            "suburb"=>$request->suburb,
            "direction"=>$request->direction,
            "city"=>$request->city,
            "district"=>$request->district,
            "state"=>$request->state,
            "country"=>$request->country
        ]);
        foreach ($employee->contacts as $contact){
            if($contact->contact_type == 1){
                DB::table('contact_mobile_number')->delete($contact->contact_id);
            } elseif ($contact->contact_type == 2){
                DB::table('contact_whatsapp_number')->delete($contact->contact_id);
            } else {
                DB::table('email_address')->delete($contact->contact_id);
            }
        }
        $employee->contacts()->delete();
        
        foreach ($request->mobile_number as $mobile_number){
            if($mobile_number != null) {
                $contact_mobile_id = DB::table('contact_mobile_number')->insertGetId(
                    array('number' => $mobile_number,
                        'isPrimary' => ($mobile_number == $request->primary_mobile) ? 1: 0)
                );
                DB::table('contacts')->insertGetId(
                    array('employee_id' => $employee->id,
                        'contact_type' => 1,
                        'contact_id' => $contact_mobile_id)
                );
            }
        }
        foreach ($request->whatsapp_number as $whatsapp_number){
            if($whatsapp_number != null) {
                $contact_whatsapp_id = DB::table('contact_whatsapp_number')->insertGetId(
                    array('number' => $whatsapp_number,
                        'isPrimary' => ($whatsapp_number == $request->primary_whatsapp) ? 1: 0)
                );
                DB::table('contacts')->insertGetId(
                    array('employee_id' => $employee->id,
                        'contact_type' => 2,
                        'contact_id' => $contact_whatsapp_id)
                );
            }
        }
        foreach ($request->email as $email){
            if($email != null) {
                $contact_email_id = DB::table('email_address')->insertGetId(
                    array('email' => $email,
                        'isPrimary' => ($email == $request->primary_email) ? 1: 0)
                );
                DB::table('contacts')->insertGetId(
                    array('employee_id' => $employee->id,
                        'contact_type' => 3,
                        'contact_id' => $contact_email_id)
                );
            }
        }
        return redirect()->route('employee.view');
        dd('ss');
    }
    public function create(){
        return view('create');
    }
    public function store(CreateEmployeeRequest $request) {
//        dd($request);
        $employee_id = Employee::create(["name"=>$request->name]);

        $address = Address::create([
            "employee_id"=>$employee_id->id,
            "address_1"=>$request->address_1,
            "address_2"=>$request->address_2,
            "location"=>$request->location,
            "postal_code"=>$request->postal_code,
            "postal_area"=>$request->postal_area,
            "taluka"=>$request->taluka,
            "suburb"=>$request->suburb,
            "direction"=>$request->direction,
            "city"=>$request->city,
            "district"=>$request->district,
            "state"=>$request->state,
            "country"=>$request->country
        ]);
        foreach ($request->mobile_number as $mobile_number){
            if($mobile_number != null) {
                $contact_mobile_id = DB::table('contact_mobile_number')->insertGetId(
                    array('number' => $mobile_number,
                        'isPrimary' => ($mobile_number == $request->primary_mobile) ? 1: 0)
                );
                DB::table('contacts')->insertGetId(
                    array('employee_id' => $employee_id->id,
                        'contact_type' => 1,
                        'contact_id' => $contact_mobile_id)
                );
            }
        }
        foreach ($request->whatsapp_number as $whatsapp_number){
            if($whatsapp_number != null) {
                $contact_whatsapp_id = DB::table('contact_whatsapp_number')->insertGetId(
                    array('number' => $whatsapp_number,
                        'isPrimary' => ($whatsapp_number == $request->primary_whatsapp) ? 1: 0)
                );
                DB::table('contacts')->insertGetId(
                    array('employee_id' => $employee_id->id,
                        'contact_type' => 2,
                        'contact_id' => $contact_whatsapp_id)
                );
            }
        }
        foreach ($request->email as $email){
            if($email != null) {
                $contact_email_id = DB::table('email_address')->insertGetId(
                    array('email' => $email,
                        'isPrimary' => ($email == $request->primary_email) ? 1: 0)
                );
                DB::table('contacts')->insertGetId(
                    array('employee_id' => $employee_id->id,
                        'contact_type' => 3,
                        'contact_id' => $contact_email_id)
                );
            }
        }
        return redirect()->route('employee.view');
    }

    public function view(){
        $employees = Employee::all();
        return view('view', compact([
            'employees'
        ]));
    }

    public function destroy(Employee $employee){
        $employee->forceDelete();
        return redirect()->route('employee.view');
    }
}
