<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $fillable = ['employee_id', 'address_1', 'address_2', 'location', 'postal_code', 'postal_area', 'taluka', 'suburb', 'East/West', 'city', 'district', 'state', 'country'];

}
