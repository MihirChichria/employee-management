<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/create', 'EmployeeController@create')->name('employee.create');
Route::get('/', function () {
    return view('view');
});

Route::post('/create/employee', "EmployeeController@store")->name('employee.store');
Route::get('/view', "EmployeeController@view")->name('employee.view');
Route::put('/edit/{employee}', "EmployeeController@update")->name('employee.update');
Route::get('/edit/{employee}', "EmployeeController@edit")->name('employee.edit');
Route::delete('/delete/{employee}', "EmployeeController@destroy")->name('employee.delete');
